import { Component, OnInit } from '@angular/core';
import { RegistroInterface } from 'src/app/models/registro.interface';
import { UserServicesService } from 'src/app/services/user-services.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PasswordValidator } from '../../password.validator';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.component.html',
  styleUrls: ['./registro.component.css'],
})
export class RegistroComponent implements OnInit {
  user: RegistroInterface = {
    email: '',
    name: '',
    firstname: '',
    lastname: '',
    password: '',
  };

  hide1 = true;
  hide2 = true;
  passwordconfirm: '';

  formul;

  constructor(
    private serv: UserServicesService,
    private fbuild: FormBuilder
  ) {
    this.formul = fbuild.group({
      email: ['', [Validators.required, Validators.email]],
      name: ['', [Validators.required, Validators.minLength(3)]],
      firstname: ['', [Validators.required, Validators.minLength(3)]],
      lastname: [''],
      password: ['', [Validators.required, Validators.minLength(8)]],
      passwordconfirm: ['', [Validators.required]],
    },
    {
      validator: PasswordValidator('password', 'passwordconfirm')
    });
  }

  ngOnInit(): void {}

  get nameInvalido() {
    return this.formul.get('name').invalid && this.formul.get('name').touched;
  }

  get firstnameInvalido() {
    return (
      this.formul.get('firstname').invalid &&
      this.formul.get('firstname').touched
    );
  }

  get emailInvalido() {
    return this.formul.get('email').invalid && this.formul.get('email').touched;
  }

  get passwordInvalido() {
    return (
      this.formul.get('password').invalid && this.formul.get('password').touched
    );
  }

  get passwordconfirmInvalido() {
    return (
      this.formul.get('passwordconfirm').invalid &&
      this.formul.get('passwordconfirm').touched
    );
  }

  registro() {
    if(this.formul.value.password != this.formul.value.passwordconfirm){
      console.log("No es lo mismo");
      this.formul.reset({
        email: this.formul.value.email,
        name: this.formul.value.name,
        firstname: this.formul.value.firstname,
        lastname: this.formul.value.lastname,
        password: this.formul.value.password,
        passwordconfirm: ""
      })
    }
    if (this.formul.invalid) {
      Object.values(this.formul.controls).forEach((fatherController) => {
        if (fatherController instanceof FormGroup) {
          Object.values(fatherController.controls).forEach(
            (childController) => {
              childController.markAsTouched();
            }
          );
        }
      });
    }
    console.log(this.formul.value);
  }
}
